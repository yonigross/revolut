# Revolut - Backend Test

This is an implementation of a RESTful service for money transfer between accounts.

## API

Default base uri `http://localhost:3000`

Creating a new account:
```
POST /accounts

POST Body:
{
    "name": <string>
}

Resposne:
<account id - string / uuid>
```

Getting account info:
```
GET /accounts/<account id>

Response:
{
    "id": <string / uuid>,
    "name": <string>,
    "balance": <decimal>
}
```

Making a transfer:

```
POST /transfers

POST Body:
{
    "from": <string>,
    "to": <string>,
    "amount": <decimal>
}

Resposne:
<account id - string / uuid>
```

## Implementation

I've implemented an Event-Sourcing for transfers/ledger.

Chose to use CQRS and eventual-consistency for account state - account "Base" details are held in a thread-safe map,
and read-only accounts state (for queries) is projected from ledger and base account details.

I've used vertx as web-server/router, but any web framework would do.

I've ignored input sanitation, authentication, edge cases such as transferring form non-existing accounts, etc, as not seem like the core of this test.


## Building

Run tests:
```
./gradlew clean test
```

Package the application:
```
./gradlew clean assemble
```

Running the application
```
./gradlew clean run
```




