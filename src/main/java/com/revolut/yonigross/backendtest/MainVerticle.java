package com.revolut.yonigross.backendtest;

import com.revolut.yonigross.backendtest.api.RestController;
import com.revolut.yonigross.backendtest.config.Config;
import com.revolut.yonigross.backendtest.ledger.GrowingArrayEventStore;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;

public class MainVerticle extends AbstractVerticle {

  private final Config config;

  public MainVerticle() {
    this.config = new Config.Builder().build();
  }

  public MainVerticle(Config config) {
    this.config = config;
  }

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    BankService bankService = new BankService(new GrowingArrayEventStore(config));

    HttpServer server = vertx.createHttpServer();

    Router router = Router.router(vertx);
    RestController controller = new RestController(bankService, router);
    controller.setRoutes();

    server.requestHandler(router)
      .listen(config.serverPort, http -> {
        if (http.succeeded()) {
          startPromise.complete();
          System.out.println("HTTP server started on port " + config.serverPort);
        } else {
          startPromise.fail(http.cause());
        }
      });
  }


}
