package com.revolut.yonigross.backendtest.account;

import com.revolut.yonigross.backendtest.ledger.Transfer;
import com.revolut.yonigross.backendtest.ledger.EventStore;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * This class encapsulate the account service. Under the hood, it uses 2 data structures for holding account details in
 * a CQRS fashion: an account details collection (which is effected by create-account commands, but is not effected by
 * transactions), and a projection of accounts state.
 *
 * Reads from projection are always safe beacuse it is immutable, and being swapped when a new projection is calculated.
 * (although they might not be up-to-date, but there is an eventual consistency
 */
public class AccountService {

  private final Map<String, Account> accounts = new ConcurrentHashMap<>();
  private Map<String, AccountView> accountsProjection = new HashMap<>();
  private final AccountBalanceProjector accountBalanceProjector;

  private final ExecutorService backgroundThread = Executors.newSingleThreadExecutor();
  private final AtomicBoolean isProjectionRunning = new AtomicBoolean(false);

  public AccountService(EventStore<Transfer> ledger) {
    accountBalanceProjector = new AccountBalanceProjector(accounts, ledger);
  }

  public void open(String accountId, String accountName) {
    Account newAccount = new Account(accountId, accountName);
    accounts.put(accountId, newAccount);
  }

  public AccountView getAccount(String accountId) {
    AccountView account = accountsProjection.get(accountId);

    return (account == null)
      ? accounts.get(accountId).generateView(0.0)
      : account;
  }

  /**
   * Run updates in a BG thread. When an update is running, new updates requests will be ignored (but iterator should
   * reach end of event store, so new event should be accumulated (but new events might be missed
   * since adding event into ledger is not atomic, and index is incremented before assignment into array.
   */
  public void updateProjection() {
    if (!isProjectionRunning.getAndSet(true)) {
      backgroundThread.submit(() -> {
        try {
          accountsProjection = accountBalanceProjector.generateProjection();
        } catch (Exception e) {
          System.out.println(e.getMessage());
        } finally {
          isProjectionRunning.set(false);
        }
      });
    }

  }

}
