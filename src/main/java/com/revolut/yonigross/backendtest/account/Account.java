package com.revolut.yonigross.backendtest.account;


/**
 * Account details (attributes that don't change much or at all, like id, name, address, etc.)
 */
public class Account {
  private final String id;
  private final String name;

  public Account(String id, String name) {
    this.id = id;
    this.name = name;
  }

  public AccountView generateView(double balance) {
    return new AccountView(id, name, balance);
  }


}
