package com.revolut.yonigross.backendtest.account;

import com.revolut.yonigross.backendtest.ledger.Transfer;
import com.revolut.yonigross.backendtest.ledger.EventStore;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Calculate the accounts state projection from ledger event store
 */
public class AccountBalanceProjector {

  private final Map<String, Account> accounts;
  private final EventStore<Transfer> ledger;

  public AccountBalanceProjector(Map<String, Account> accounts, EventStore<Transfer> ledger) {
    this.accounts = accounts;
    this.ledger = ledger;
  }

  public Map<String, AccountView> generateProjection() {
    Map<String, Double> calculatedBalances = new HashMap<>();
    accounts.forEach((id, account) -> calculatedBalances.put(id, 0.0));

    calculateBalances(calculatedBalances);

    return mergeAccountsWithBalance(calculatedBalances);
  }

  private void calculateBalances(Map<String, Double> calculatedBalances) {
    Iterator<Transfer> it = ledger.iterator();
    while (it.hasNext()) {
      Transfer transfer = it.next();

      if (transfer != null) {
        Double sourceAccountBalance = calculatedBalances.get(transfer.sourceAccountId);
        Double targetAccountBalance = calculatedBalances.get(transfer.targetAccountId);

        double sourceAccountNewBalance = sourceAccountBalance != null ? sourceAccountBalance - transfer.amount
          : -transfer.amount;

        double targetAccountNewBalance = targetAccountBalance != null ? targetAccountBalance + transfer.amount
          : transfer.amount;

        calculatedBalances.put(transfer.sourceAccountId, sourceAccountNewBalance);
        calculatedBalances.put(transfer.targetAccountId, targetAccountNewBalance);
      }

    }
  }

  private Map<String, AccountView> mergeAccountsWithBalance(Map<String, Double> calculatedBalances) {
    Map<String, AccountView> newProjection = new HashMap<>();
    accounts.forEach((id, account)-> {
      Double balance = calculatedBalances.get(id);
      AccountView accountView = account.generateView(balance);
      newProjection.put(id, accountView);
    });
    return newProjection;
  }

}
