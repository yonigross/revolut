package com.revolut.yonigross.backendtest.account;

import io.vertx.core.json.JsonObject;

/**
 * An immutable representation of bank account state at a certain time
 */
public class AccountView {
  public final String id;
  public final String name;

  /*
    having balance in a finance system as a double is a bad idea, would use int (to represent cents), but chose double
    for simplicity (as instructed)
   */
  public final double balance;

  AccountView(String id, String name, double balance) {
    this.id = id;
    this.name = name;
    this.balance = balance;
  }

  public String toJson() {
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("id", id);
    jsonObject.put("name", name);
    jsonObject.put("balance", balance);
    return jsonObject.toString();
  }

}
