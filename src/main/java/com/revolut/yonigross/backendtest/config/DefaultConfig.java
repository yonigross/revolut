package com.revolut.yonigross.backendtest.config;

public class DefaultConfig {
  public static final int SERVER_PORT = 3000;
  public static int EVENT_STORE_SIZE = 1 << 12;
}
