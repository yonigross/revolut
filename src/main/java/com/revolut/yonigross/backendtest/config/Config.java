package com.revolut.yonigross.backendtest.config;

public class Config {

  public final int serverPort;
  public final int eventStoreSize;

  private Config(Builder builder) {
    this.serverPort = builder.serverPort;
    this.eventStoreSize = builder.eventStoreSize;
  }

  public static class Builder {
    private int serverPort;
    private int eventStoreSize;

    public Builder() {
      this.serverPort = DefaultConfig.SERVER_PORT;
      this.eventStoreSize = DefaultConfig.EVENT_STORE_SIZE;
    }

    public Builder setServerPort(int serverPort) {
      this.serverPort = serverPort;
      return this;
    }

    public Builder setEventStoreSize(int eventStoreSize) {
      this.eventStoreSize = eventStoreSize;
      return this;
    }

    public Config build() {
      return new Config(this);
    }
  }
}
