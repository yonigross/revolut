package com.revolut.yonigross.backendtest.ledger;

public class Transfer {
  public final String sourceAccountId;
  public final String targetAccountId;
  public final double amount;

  public Transfer(String fromAccountId, String toAccountId, double amount) {
    this.sourceAccountId = fromAccountId;
    this.targetAccountId = toAccountId;
    this.amount = amount;
  }
}
