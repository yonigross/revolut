package com.revolut.yonigross.backendtest.ledger;

import java.util.Iterator;

public interface EventStore<T> {
  void append(T event);
  Iterator<T> iterator();
}
