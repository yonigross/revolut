package com.revolut.yonigross.backendtest.ledger;

import com.revolut.yonigross.backendtest.config.Config;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class GrowingArrayEventStore implements EventStore<Transfer> {
  private final Map<Integer, Transfer[]> ledger = new ConcurrentHashMap<>();
  private final AtomicLong counter = new AtomicLong(0);
  private final int bufferSize;

  public GrowingArrayEventStore(Config config) {
    this.bufferSize = config.eventStoreSize;
    ledger.put(0, new Transfer[bufferSize]);
  }

  @Override
  public void append(Transfer transfer) {
    /*
      note: this is not atomic and in theory we can have our iterator reaching the last index before it's been assigned,
      but projection can deal with it (ignore nulls)
     */

    long index = counter.getAndIncrement();
    int batch = (int) (index / bufferSize);
    int innerIndex = (int) (index % bufferSize);

    if (innerIndex == 1) {  //when starting to fill a new buffer, prepare the next one
      ledger.put(batch + 1, (new Transfer[bufferSize]));
    }


    ledger.get(batch)[innerIndex] = transfer;
  }

  @Override
  public Iterator<Transfer> iterator() {
    return new Itr();
  }

  private class Itr implements java.util.Iterator<Transfer> {
    private long index = 0;
    private int innerIndex = 0;
    private Transfer[] innerLedger = ledger.get(0);

    @Override
    public boolean hasNext() {
      return index < counter.get();
    }

    @Override
    public Transfer next() {
      if (innerIndex == bufferSize) {
        innerIndex = 0;
        int batch = (int) (index / bufferSize);
        innerLedger = ledger.get(batch);
      }

      Transfer item = innerLedger[innerIndex];

      index++;
      innerIndex++;

      return item;
    }
  }
}
