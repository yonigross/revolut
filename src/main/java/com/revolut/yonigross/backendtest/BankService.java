package com.revolut.yonigross.backendtest;

import com.revolut.yonigross.backendtest.account.AccountService;
import com.revolut.yonigross.backendtest.account.AccountView;
import com.revolut.yonigross.backendtest.ledger.EventStore;
import com.revolut.yonigross.backendtest.ledger.Transfer;

import java.util.Arrays;
import java.util.UUID;

/**
 * The bank service holds the ledger as an event store of transfers, and the accounts as described in the account service.
 */
public class BankService {

  private final EventStore<Transfer> ledger;
  private final AccountService accounts;

  public BankService(EventStore<Transfer> ledger) {
    this.ledger = ledger;
    this.accounts = new AccountService(ledger);
  }

  public String createAccount(String accountName) {
    String accountId = UUID.randomUUID().toString();
    accounts.open(accountId, accountName);
    return accountId;
  }

  public AccountView getAccount(String accountId) {
    return accounts.getAccount(accountId);
  }

  public void doTransfer(String fromAccount, String toAccount, Double amount) {
    if (validAccounts(fromAccount, toAccount)){
      Transfer transfer = new Transfer(fromAccount, toAccount, amount);
      ledger.append(transfer);
      accounts.updateProjection();
    }

  }

  private boolean validAccounts(String... accountsIdsToValidate) {
    return Arrays.stream(accountsIdsToValidate)
      .map(accountId -> accounts.getAccount(accountId) != null)
      .reduce(true, (isValid1, isValid2) -> isValid1 && isValid2);
  }
}
