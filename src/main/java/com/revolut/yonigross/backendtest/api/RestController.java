package com.revolut.yonigross.backendtest.api;

import com.revolut.yonigross.backendtest.BankService;
import com.revolut.yonigross.backendtest.account.AccountView;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class RestController {

  private final BankService bankService;
  private final Router router;

  public RestController(BankService bankService, Router router) {
    this.bankService = bankService;
    this.router = router;
  }

  public void setRoutes() {
    router.route().handler(BodyHandler.create());
    router.post("/accounts").handler(this::createAccount);
    router.get("/accounts/:accountId").handler(this::getAccountDetails);

    router.post("/transfers").handler(this::transfer);

    router.get("/").handler(this::rootHandler);
  }

  public void createAccount(RoutingContext routingContext) {
    String name = extractAccountName(routingContext);
    if (name == null) {
      routingContext.response().end("Please provide account name");
    }

    String accountId = bankService.createAccount(name);
    routingContext.response().end(accountId);
  }

  public void getAccountDetails(RoutingContext routingContext) {
    String accountId = routingContext.pathParam("accountId");
    AccountView account = bankService.getAccount(accountId);
    routingContext.response().end(account.toJson());
  }

  private String extractAccountName(RoutingContext routingContext) {
    @Nullable JsonObject json = routingContext.getBodyAsJson();
    if (json != null) {
      return json.getString("name");
    }
    return null;
  }

  public void transfer(RoutingContext routingContext) {
    @Nullable JsonObject json = routingContext.getBodyAsJson();
    if (json != null) {
      String from = json.getString("from");
      String to = json.getString("to");
      Double amount = json.getDouble("amount");
      bankService.doTransfer(from, to, amount);
      routingContext.response().end("Transfer is being processed");
    } else {
      routingContext.response().end("bad request");
    }

  }

  public void rootHandler(RoutingContext routingContext) {
    routingContext.response()
      .putHeader("content-type", "text/plain")
      .end("Revolut Backend test, Yoni Gross");
  }
}
