package com.revolut.yonigross.backendtest;

import com.revolut.yonigross.backendtest.config.Config;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static com.revolut.yonigross.backendtest.TestAccount.createAccount;
import static com.revolut.yonigross.backendtest.TestAccount.getAccountDetails;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(VertxExtension.class)
public class TestTransfer {

  private Config config;

  @BeforeEach
  void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
    config = new Config.Builder().setEventStoreSize(16).build();
    vertx.deployVerticle(new MainVerticle(config), testContext.succeeding(id -> testContext.completeNow()));
  }

  @Test
  void single_transfer_from_a_to_b(Vertx vertx, VertxTestContext testContext) throws Throwable {
    HttpClient client = HttpClient.newHttpClient();
    String accountFromId = createAccount(config, client, "From account");
    String accountToId = createAccount(config, client, "To account");

    Double amount = 1.50;

    makeTransfer(client, accountFromId, accountToId, amount);

    Thread.sleep(200);

    JsonObject accountFrom = new JsonObject(getAccountDetails(config, client, accountFromId));
    JsonObject accountTo = new JsonObject(getAccountDetails(config, client, accountToId));

    assertEquals(amount, accountTo.getDouble("balance"));
    assertEquals(-amount, accountFrom.getDouble("balance"));

    testContext.completeNow();
  }

  @Test
  void multi_transfers_from_a_to_b(Vertx vertx, VertxTestContext testContext) throws Throwable {
    HttpClient client = HttpClient.newHttpClient();
    String accountAId = createAccount(config, client, "A");
    String accountBId = createAccount(config, client, "B");
    String accountCId = createAccount(config, client, "B");

    Double amountAtoB = 3.0;
    Double amountBtoC = 1.0;

    int iterations = 48;

    for (int i = 0; i < iterations; i++) {
      makeTransfer(client, accountAId, accountBId, amountAtoB);
      makeTransfer(client, accountBId, accountCId, amountBtoC);
    }

    Thread.sleep(1000);

    JsonObject accountB = new JsonObject(getAccountDetails(config, client, accountBId));
    JsonObject accountC = new JsonObject(getAccountDetails(config, client, accountCId));

    Double expectedAtB = (amountAtoB - amountBtoC) * iterations;
    Double expectedAtC = amountBtoC * iterations;

    assertEquals(expectedAtB, accountB.getDouble("balance"));
    assertEquals(expectedAtC, accountC.getDouble("balance"));

    testContext.completeNow();
  }

  private void makeTransfer(HttpClient client, String accountFromId, String accountToId, Double amount) throws Exception {
    JsonObject transferJson = new JsonObject();
    transferJson.put("from", accountFromId);
    transferJson.put("to", accountToId);
    transferJson.put("amount", amount);

    HttpRequest request = HttpRequest.newBuilder()
      .uri(URI.create("http://localhost:" + config.serverPort + "/transfers"))
      .POST(HttpRequest.BodyPublishers.ofString(transferJson.toString()))
      .build();

    client.sendAsync(request, HttpResponse.BodyHandlers.ofString());

  }


}
