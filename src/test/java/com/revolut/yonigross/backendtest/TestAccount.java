package com.revolut.yonigross.backendtest;

import com.revolut.yonigross.backendtest.config.Config;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(VertxExtension.class)
public class TestAccount {

  private HttpClient client;
  private Config config;

  @BeforeEach
  void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
    config = new Config.Builder().build();
    vertx.deployVerticle(new MainVerticle(config), testContext.succeeding(id -> testContext.completeNow()));
    client = java.net.http.HttpClient.newHttpClient();
  }

  @Test
  void account_created(Vertx vertx, VertxTestContext testContext) throws Throwable {
    HttpRequest request = generateCreateAccountRequest(config, "account1");

    HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

    assertEquals(200, response.statusCode());
    String accountUuidString = response.body();
    UUID accountUuid = UUID.fromString(accountUuidString);
    testContext.completeNow();
  }

  @Test
  void get_account_balance(Vertx vertx, VertxTestContext testContext) throws Throwable {
    final String accountName = "New Account";
    String accountId = createAccount(config, client, accountName);

    String accountDetailsJson = getAccountDetails(config, client, accountId);
    JsonObject accountDetails = new JsonObject(accountDetailsJson);

    assertEquals(accountName, accountDetails.getString("name"));
    assertEquals(0, accountDetails.getDouble("balance"));

    testContext.completeNow();
  }

  public static String getAccountDetails(Config config, HttpClient client, String accountId) throws Throwable {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(URI.create("http://localhost:" + config.serverPort + "/accounts/" + accountId))
      .GET()
      .build();
    HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
    assertEquals(200, response.statusCode());
    return response.body();
  }

  private static HttpRequest generateCreateAccountRequest(Config config, String accountName) {
    JsonObject accountJson = new JsonObject();
    accountJson.put("name", accountName);

    return HttpRequest.newBuilder()
      .uri(URI.create("http://localhost:" + config.serverPort + "/accounts"))
      .POST(HttpRequest.BodyPublishers.ofString(accountJson.toString()))
      .build();
  }

  public static String createAccount(Config config, HttpClient client, String accountName) throws Throwable {
    HttpRequest request = generateCreateAccountRequest(config, accountName);
    HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
    return response.body();

  }
}
