package com.revolut.yonigross.backendtest.ledger;

import com.revolut.yonigross.backendtest.MainVerticle;
import com.revolut.yonigross.backendtest.config.Config;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.revolut.yonigross.backendtest.TestAccount.createAccount;
import static com.revolut.yonigross.backendtest.TestAccount.getAccountDetails;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestEventStore {

  private Config config;

  @BeforeEach
  void default_config() {
    config = new Config.Builder().build();
  }

  @Test
  void empty_store_has_no_elements() throws Throwable {
    EventStore<Transfer> store = new GrowingArrayEventStore(config);

    Iterator<Transfer> it = store.iterator();

    assertEquals(false, it.hasNext());
  }

  @Test
  void store_iterator_one_element() throws Throwable {
    EventStore<Transfer> store = new GrowingArrayEventStore(config);
    Transfer transfer = new Transfer("A", "B", 1.0);
    store.append(transfer);

    Iterator<Transfer> it = store.iterator();
    assertEquals(true, it.hasNext());
    Transfer storedTransfer = it.next();
    assertEquals(false, it.hasNext());
    assertEquals(transfer, storedTransfer);
  }

  @Test
  void ledger_grows_when_exceeding_size() throws Throwable {
    config = new Config.Builder().setEventStoreSize(10).build();
    EventStore<Transfer> store = new GrowingArrayEventStore(config);
    int numberOfTransfers = 100;
    double amount = 10.0;
    for (int i = 0; i < numberOfTransfers; i++) {
      Transfer transfer = new Transfer("A", "B", amount);
      store.append(transfer);
    }

    int counter = 0;
    double accumulator = 0.0;

    Iterator<Transfer> it = store.iterator();
    while (it.hasNext()) {
      counter++;
      accumulator += it.next().amount;
    }

    assertEquals(numberOfTransfers, counter);
    assertEquals(numberOfTransfers * amount, accumulator);
  }

  @Test
  void multi_threaded_heavy_load_insertion() throws Throwable {
    EventStore<Transfer> store = new GrowingArrayEventStore(config);


    int numberOfTransfers = 1 << 20;
    double amount = 10.0;

    ExecutorService threadPool = Executors.newFixedThreadPool(8);

    for (int i = 0; i < numberOfTransfers; i++) {
      threadPool.submit(() -> {
        Transfer transfer = new Transfer("A", "B", amount);
        store.append(transfer);
      });
    }

    Thread.sleep(1000);

    int counter = 0;
    double accumulator = 0.0;

    Iterator<Transfer> it = store.iterator();
    while (it.hasNext()) {
      counter++;
      accumulator += it.next().amount;
    }

    assertEquals(numberOfTransfers, counter);
    assertEquals(numberOfTransfers * amount, accumulator);
  }


}
