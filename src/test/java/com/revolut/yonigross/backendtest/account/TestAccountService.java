package com.revolut.yonigross.backendtest.account;

import com.revolut.yonigross.backendtest.config.Config;
import com.revolut.yonigross.backendtest.ledger.EventStore;
import com.revolut.yonigross.backendtest.ledger.GrowingArrayEventStore;
import com.revolut.yonigross.backendtest.ledger.Transfer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAccountService {

  private EventStore<Transfer> ledger;
  private AccountService accountService;

  @BeforeEach
  void default_config() {
    ledger = new GrowingArrayEventStore(new Config.Builder().build());
    accountService = new AccountService(ledger);
  }

  @Test
  void account_with_no_transactions() throws Throwable {
    accountService.open("1", "NAME");
    AccountView accountView = accountService.getAccount("1");

    assertEquals("1", accountView.id);
    assertEquals("NAME", accountView.name);
    assertEquals(0.0, accountView.balance);
  }

  @Test
  void account_with_transactions() throws Throwable {
    String accountId = "1";
    double amount = 1.0;
    accountService.open(accountId, "NAME");
    accountService.open("BANK", "BANK");
    ledger.append(new Transfer("BANK", accountId, 1.0));

    accountService.updateProjection();
    Thread.sleep(10);

    AccountView accountView = accountService.getAccount(accountId);
    assertEquals(accountId, accountView.id);
    assertEquals("NAME", accountView.name);
    assertEquals(amount, accountView.balance);
  }

  @Test
  void account_with_anonymous_transaction() throws Throwable {
    String accountId = "1";
    double amount = 1.0;
    accountService.open(accountId, "NAME");
    ledger.append(new Transfer("NO ONE", accountId, 1.0));

    accountService.updateProjection();
    Thread.sleep(10);

    AccountView accountView = accountService.getAccount(accountId);
    assertEquals(accountId, accountView.id);
    assertEquals("NAME", accountView.name);
    assertEquals(amount, accountView.balance);
  }


  @Test
  void multi_threaded_heavy_load_insertion() throws Throwable {
    final String accountId = "1";
    final double amount = 1.0;
    int numberOfTransfers = 1 << 20;
    accountService.open(accountId, "NAME");


    ExecutorService threadPool = Executors.newFixedThreadPool(8);

    for (int i = 0; i < numberOfTransfers; i++) {
      threadPool.submit(() -> {
        ledger.append(new Transfer("NO ONE", accountId, amount));
        accountService.updateProjection();
      });
    }

    Thread.sleep(1000);

    AccountView accountView = accountService.getAccount(accountId);
    assertEquals(numberOfTransfers * amount, accountView.balance);
  }


}
